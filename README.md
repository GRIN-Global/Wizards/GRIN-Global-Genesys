## **GRIN-Global Curator Tool - Genesys integration**

The Genesys-GRIN-Global integration wizard will allow to publishing the passport data to the Genesys and, as a wizard, the Curator Tool will not require coding changes.

## How to install the Genesys Wizard for Curator Tool

1. Download the files: "GenesysWizard.dll", "GeneSys2.Client.dll", "Newtonsoft.Json.dll" and "stdole.dll" from [here](https://gitlab.com/GRIN-Global/Wizards/GRIN-Global-Genesys/-/tree/master/GenesysWizard/Resources).
1. Unblock the DLLs files. Right click the DLL file, select properties and check "Unblock"
1. Copy the file "GenesysWizard.dll" to "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool\Wizards" folder.
1. Copy the files "GeneSys2.Client.dll", "Newtonsoft.Json.dll" and "stdole.dll" to "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool" folder.
1. Run Curator Tool.

## Importing the get_passport_dataview

Don't forget to import the dataview **get_passport_data** through GRIN-Global Admin-Tool, the dataview get_pasport_data is available [here](https://gitlab.com/GRIN-Global/Wizards/GRIN-Global-Genesys/-/blob/master/GenesysWizard/Resources/get_passport_data.dataviewxml) 


The get_passport_dataview gets the GRIN-Global URL from the app_setting table, execute this script to add the URL to app_setting table.


`begin transaction`

`insert into app_setting(category_tag,name,value,created_date,created_by,owned_date,owned_by)`

`values('URL','GRIN-Global URL','http://my.domain.org/gringlobal',getdate(),48,getdate(),48)`

`commit transaction`
