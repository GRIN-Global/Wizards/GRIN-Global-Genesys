﻿using GeneSys2.Client.OAuth;
using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesysWizard
{
    /// <summary>
    /// Enumeration of Genesys connectivity test results. 
    /// </summary>
    public enum ConnectivityTestResult
    {
        Unknown = 0,
        Success,
        Unauthorized,
        NoConnection,
        ServiceUnavailable
    }

    /// <summary>
    /// Genesys ISyncController interface for interacting with the Genesys server
    /// </summary>
    public interface ISyncController
    {
        /// <summary>
        /// OAuth token storage
        /// </summary>
        SecureTokenStorage TokenStorage { get; }

        /// <summary>
        /// Authentication provider
        /// </summary>
        OAuthHandler AuthProvider { get; }

        /// <summary>
        /// GRINGlobal Client Shared Utilities
        /// </summary>
        SharedUtils SharedUtils { get; }

        /// <summary>
        /// Address of the Genesys server
        /// </summary>
        string ServerAddress { get; }

        /// <summary>
        /// Update the OAuth Client ID and Secret
        /// </summary>
        /// <param name="serverAddress">URI of the Genesys server</param>
        /// <param name="clientId">OAuth Client Id</param>
        /// <param name="clientSecret">OAuth Client Secret</param>
        void UpdateClientID(Uri serverAddress, string clientId, string clientSecret);

        /// <summary>
        /// Check whether there is connectivity to the Genesys server
        /// </summary>
        /// <returns></returns>
        ConnectivityTestResult CheckConnectivity();

        /// <summary>
        /// Launch the configuration wizard to setup the Genesys OAuth parameters
        /// </summary>
        void Configure();
    }
}
