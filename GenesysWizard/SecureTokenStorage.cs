﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneSys2.Client.OAuth;
using Newtonsoft.Json;
using System.IO;
using GeneSys2.Client.Model;
using System.Security.Cryptography;

namespace GenesysWizard
{
    /// <summary>
    /// Genesys Token Storage which is encrypted and stored to 
    /// a file.  This implementation also stores the client ID
    /// and secret.
    /// </summary>
    public class SecureTokenStorage : ITokenStorage 
    {
        private const string PROD_GENESYS_SERVER = "https://www.genesys-pgr.org";
        private string _clientKey;
        private string _clientSecret;
        private string _accessToken;
        private string _refreshToken;
        private string _fileName;
        private Uri _serverAddress;
        private bool _loading = false;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">File used to store the tokens</param>
        public SecureTokenStorage(string fileName)
        {
            this._fileName = fileName;
            Load();
        }
        /// <summary>
        /// Method to inject the unity test
        /// </summary>
        /// <param name="serverAddress">Fake uri</param>
        public void InjectTest(Uri serverAddress)
        {
            this._serverAddress = serverAddress;
        }

        /// <summary>
        /// OAuth Client Key
        /// </summary>
        [JsonProperty(PropertyName = "clientKey")]
        public string ClientKey
        {
            get
            {
                return _clientKey;
            }
            set
            {
                // If the token changes, save the file
                if (string.Compare(value, _clientKey) != 0)
                {
                    _clientKey = value;
                    Save();
                }
            }
        }

        /// <summary>
        /// OAuth Client Secret
        /// </summary>
        [JsonProperty(PropertyName = "clientSecret")]
        public string ClientSecret
        {
            get
            {
                return _clientSecret;
            }
            set
            {
                // If the token changes, save the file
                if (string.Compare(value, _clientSecret) != 0)
                {
                    _clientSecret = value;
                    Save();
                }
            }
        }

        /// <summary>
        /// OAuth Access Token
        /// </summary>
        [JsonProperty(PropertyName = "accessToken")]
        public string AccessToken
        {
            get
            {
                return _accessToken;
            }
            set
            {
                // If the token changes, save the file
                if (string.Compare(value, _accessToken) != 0)
                {
                    _accessToken = value;
                    Save();
                }
            }
        }

        /// <summary>
        /// OAuth Refresh Token
        /// </summary>
        [JsonProperty(PropertyName = "refreshToken")]
        public string RefreshToken
        {
            get
            {
                return _refreshToken;
            }
            set
            {
                // If the token changes, save the file
                if (string.Compare(value, _refreshToken) != 0)
                {
                    _refreshToken = value;
                    Save();
                }
            }
        }

        /// <summary>
        /// Filename used as a backing storage
        /// </summary>
        protected string FileName
        {
            get
            {
                return _fileName;
            }
        }

        [JsonProperty(PropertyName = "serverAddress")]
        public Uri ServerAddress
        {
            get 
            {
                return _serverAddress;
            }
            set
            {
                // If the token changes, save the file
                if (!_serverAddress.Equals(value))
                {
                    _serverAddress = value;
                    Save();
                }
            }
        }

        /// <summary>
        /// Loads the values from the file
        /// </summary>
        protected void Load()
        {
            if (File.Exists(FileName))
            {
                try
                {
                    _loading = true;

                    byte[] unencrypted = System.Security.Cryptography.ProtectedData.Unprotect(File.ReadAllBytes(FileName), null, System.Security.Cryptography.DataProtectionScope.CurrentUser);
                    GenericObject values = JsonConvert.DeserializeObject<GenericObject>(System.Text.Encoding.UTF8.GetString(unencrypted));

                    if (values.Keys.Contains("accessToken") && values["accessToken"] != null)
                        this.AccessToken = values["accessToken"].ToString();
                    if (values.Keys.Contains("refreshToken") && values["refreshToken"] != null)
                        this.RefreshToken = values["refreshToken"].ToString();
                    if (values.Keys.Contains("clientKey") && values["clientKey"] != null)
                        this._clientKey = values["clientKey"].ToString();
                    if (values.Keys.Contains("clientSecret") && values["clientSecret"] != null)
                        this._clientSecret = values["clientSecret"].ToString();
                    if (values.Keys.Contains("serverAddress") && values["serverAddress"] != null)
                        this._serverAddress = new Uri(values["serverAddress"].ToString());
                    else
                        this._serverAddress = new Uri(PROD_GENESYS_SERVER);
                }
                catch (CryptographicException)
                {
                    // We are intentionally swallowing this exception. If the source file is malformed, the 
                    // implementation will simply prompt the user to setup the authentication again.
                }
                finally
                {
                    _loading = false;
                }
            }
            else
            {
                this._serverAddress = new Uri(PROD_GENESYS_SERVER);
            }
        }

        /// <summary>
        /// Encrypt and save the tokens back to the file
        /// </summary>
        protected void Save()
        {
            if (!_loading)
            {
                string data = JsonConvert.SerializeObject(this);
                byte[] protectedData = ProtectedData.Protect(Encoding.UTF8.GetBytes(data), null, DataProtectionScope.CurrentUser);
                File.WriteAllBytes(FileName, protectedData);
            }
        }
    }
}
