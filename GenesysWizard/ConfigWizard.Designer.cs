﻿namespace GenesysWizard
{
    partial class GenesysConfigWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenesysConfigWizard));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.pnlWelcome = new System.Windows.Forms.Panel();
            this.pbGenesysLogo = new System.Windows.Forms.PictureBox();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.pnlClientInfo = new System.Windows.Forms.Panel();
            this.txtServerAddress = new System.Windows.Forms.TextBox();
            this.lblServerAddress = new System.Windows.Forms.Label();
            this.lblClientInfoInstructions = new System.Windows.Forms.Label();
            this.txtClientSecret = new System.Windows.Forms.TextBox();
            this.txtClientKey = new System.Windows.Forms.TextBox();
            this.lblClientSecret = new System.Windows.Forms.Label();
            this.lblClientKey = new System.Windows.Forms.Label();
            this.pnlVerificationCode = new System.Windows.Forms.Panel();
            this.btnGenVerificationCode = new System.Windows.Forms.Button();
            this.lblVerificationCodeInstructions = new System.Windows.Forms.Label();
            this.lblVerificationCode = new System.Windows.Forms.Label();
            this.txtVerificationCode = new System.Windows.Forms.TextBox();
            this.pnlConfigSuccess = new System.Windows.Forms.Panel();
            this.lblConfigSuccess = new System.Windows.Forms.Label();
            this.pnlFooter.SuspendLayout();
            this.pnlWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbGenesysLogo)).BeginInit();
            this.pnlClientInfo.SuspendLayout();
            this.pnlVerificationCode.SuspendLayout();
            this.pnlConfigSuccess.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            resources.ApplyResources(this.btnNext, "btnNext");
            this.btnNext.Name = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            resources.ApplyResources(this.btnPrevious, "btnPrevious");
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // pnlFooter
            // 
            this.pnlFooter.Controls.Add(this.btnPrevious);
            this.pnlFooter.Controls.Add(this.btnNext);
            resources.ApplyResources(this.pnlFooter, "pnlFooter");
            this.pnlFooter.Name = "pnlFooter";
            // 
            // pnlWelcome
            // 
            this.pnlWelcome.Controls.Add(this.pbGenesysLogo);
            this.pnlWelcome.Controls.Add(this.lblWelcome);
            resources.ApplyResources(this.pnlWelcome, "pnlWelcome");
            this.pnlWelcome.Name = "pnlWelcome";
            // 
            // pbGenesysLogo
            // 
            resources.ApplyResources(this.pbGenesysLogo, "pbGenesysLogo");
            this.pbGenesysLogo.Name = "pbGenesysLogo";
            this.pbGenesysLogo.TabStop = false;
            // 
            // lblWelcome
            // 
            resources.ApplyResources(this.lblWelcome, "lblWelcome");
            this.lblWelcome.Name = "lblWelcome";
            // 
            // pnlClientInfo
            // 
            this.pnlClientInfo.Controls.Add(this.txtServerAddress);
            this.pnlClientInfo.Controls.Add(this.lblServerAddress);
            this.pnlClientInfo.Controls.Add(this.lblClientInfoInstructions);
            this.pnlClientInfo.Controls.Add(this.txtClientSecret);
            this.pnlClientInfo.Controls.Add(this.txtClientKey);
            this.pnlClientInfo.Controls.Add(this.lblClientSecret);
            this.pnlClientInfo.Controls.Add(this.lblClientKey);
            resources.ApplyResources(this.pnlClientInfo, "pnlClientInfo");
            this.pnlClientInfo.Name = "pnlClientInfo";
            this.pnlClientInfo.VisibleChanged += new System.EventHandler(this.pnlClientInfo_VisibleChanged);
            // 
            // txtServerAddress
            // 
            resources.ApplyResources(this.txtServerAddress, "txtServerAddress");
            this.txtServerAddress.Name = "txtServerAddress";
            this.txtServerAddress.TextChanged += new System.EventHandler(this.common_CheckNextButtonState);
            this.txtServerAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.common_CheckNextButtonState);
            // 
            // lblServerAddress
            // 
            resources.ApplyResources(this.lblServerAddress, "lblServerAddress");
            this.lblServerAddress.Name = "lblServerAddress";
            // 
            // lblClientInfoInstructions
            // 
            resources.ApplyResources(this.lblClientInfoInstructions, "lblClientInfoInstructions");
            this.lblClientInfoInstructions.Name = "lblClientInfoInstructions";
            // 
            // txtClientSecret
            // 
            resources.ApplyResources(this.txtClientSecret, "txtClientSecret");
            this.txtClientSecret.Name = "txtClientSecret";
            this.txtClientSecret.TextChanged += new System.EventHandler(this.common_CheckNextButtonState);
            this.txtClientSecret.KeyDown += new System.Windows.Forms.KeyEventHandler(this.common_CheckNextButtonState);
            // 
            // txtClientKey
            // 
            resources.ApplyResources(this.txtClientKey, "txtClientKey");
            this.txtClientKey.Name = "txtClientKey";
            this.txtClientKey.TextChanged += new System.EventHandler(this.common_CheckNextButtonState);
            this.txtClientKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.common_CheckNextButtonState);
            // 
            // lblClientSecret
            // 
            resources.ApplyResources(this.lblClientSecret, "lblClientSecret");
            this.lblClientSecret.Name = "lblClientSecret";
            // 
            // lblClientKey
            // 
            resources.ApplyResources(this.lblClientKey, "lblClientKey");
            this.lblClientKey.Name = "lblClientKey";
            // 
            // pnlVerificationCode
            // 
            this.pnlVerificationCode.Controls.Add(this.btnGenVerificationCode);
            this.pnlVerificationCode.Controls.Add(this.lblVerificationCodeInstructions);
            this.pnlVerificationCode.Controls.Add(this.lblVerificationCode);
            this.pnlVerificationCode.Controls.Add(this.txtVerificationCode);
            resources.ApplyResources(this.pnlVerificationCode, "pnlVerificationCode");
            this.pnlVerificationCode.Name = "pnlVerificationCode";
            // 
            // btnGenVerificationCode
            // 
            resources.ApplyResources(this.btnGenVerificationCode, "btnGenVerificationCode");
            this.btnGenVerificationCode.Name = "btnGenVerificationCode";
            this.btnGenVerificationCode.UseVisualStyleBackColor = true;
            this.btnGenVerificationCode.Click += new System.EventHandler(this.btnGenVerificationCode_Click);
            // 
            // lblVerificationCodeInstructions
            // 
            resources.ApplyResources(this.lblVerificationCodeInstructions, "lblVerificationCodeInstructions");
            this.lblVerificationCodeInstructions.Name = "lblVerificationCodeInstructions";
            // 
            // lblVerificationCode
            // 
            resources.ApplyResources(this.lblVerificationCode, "lblVerificationCode");
            this.lblVerificationCode.Name = "lblVerificationCode";
            // 
            // txtVerificationCode
            // 
            resources.ApplyResources(this.txtVerificationCode, "txtVerificationCode");
            this.txtVerificationCode.Name = "txtVerificationCode";
            this.txtVerificationCode.TextChanged += new System.EventHandler(this.common_CheckNextButtonState);
            this.txtVerificationCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.common_CheckNextButtonState);
            // 
            // pnlConfigSuccess
            // 
            this.pnlConfigSuccess.Controls.Add(this.lblConfigSuccess);
            resources.ApplyResources(this.pnlConfigSuccess, "pnlConfigSuccess");
            this.pnlConfigSuccess.Name = "pnlConfigSuccess";
            // 
            // lblConfigSuccess
            // 
            resources.ApplyResources(this.lblConfigSuccess, "lblConfigSuccess");
            this.lblConfigSuccess.Name = "lblConfigSuccess";
            // 
            // GenesysConfigWizard
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlClientInfo);
            this.Controls.Add(this.pnlWelcome);
            this.Controls.Add(this.pnlConfigSuccess);
            this.Controls.Add(this.pnlVerificationCode);
            this.Controls.Add(this.pnlFooter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GenesysConfigWizard";
            this.Load += new System.EventHandler(this.ConfigWizard_Load);
            this.pnlFooter.ResumeLayout(false);
            this.pnlWelcome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbGenesysLogo)).EndInit();
            this.pnlClientInfo.ResumeLayout(false);
            this.pnlClientInfo.PerformLayout();
            this.pnlVerificationCode.ResumeLayout(false);
            this.pnlVerificationCode.PerformLayout();
            this.pnlConfigSuccess.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlWelcome;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Panel pnlClientInfo;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Label lblClientSecret;
        private System.Windows.Forms.Label lblClientKey;
        private System.Windows.Forms.Label lblClientInfoInstructions;
        private System.Windows.Forms.TextBox txtClientSecret;
        private System.Windows.Forms.TextBox txtClientKey;
        private System.Windows.Forms.Panel pnlVerificationCode;
        private System.Windows.Forms.Label lblVerificationCode;
        private System.Windows.Forms.TextBox txtVerificationCode;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Button btnGenVerificationCode;
        private System.Windows.Forms.Label lblVerificationCodeInstructions;
        private System.Windows.Forms.Panel pnlConfigSuccess;
        private System.Windows.Forms.PictureBox pbGenesysLogo;
        private System.Windows.Forms.Label lblConfigSuccess;
        private System.Windows.Forms.TextBox txtServerAddress;
        private System.Windows.Forms.Label lblServerAddress;
    }
}