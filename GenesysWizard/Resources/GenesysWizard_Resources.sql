BEGIN TRANSACTION

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard','Text', 'Genesys Wizard v{0}', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysConfigWizard','Text', 'Genesys Configuration', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_missingPassportFieldsMessage','Text', 'Genesys Wizard Dataview Error', 48,getdate(),48,getdate())
  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_missingPassportFieldsMessage','ux_textboxMessage', 'The data view "{0}" is missing the following required passport elements: \n\n      {1}\n\nGenesys data transfer will not be possible.', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_connectivityUnauthorizedMessage','Text', 'Genesys Connection Error', 48,getdate(),48,getdate())
  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_connectivityUnauthorizedMessage','ux_textboxMessage', 'Your Genesys connection has not been configured or you are no longer authorized. Please select Configure to resolve.', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_connectivityNoConnectionMessage','Text', 'Genesys Connection Error', 48,getdate(),48,getdate())
  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_connectivityNoConnectionMessage','ux_textboxMessage', 'No connection to Genesys. Please verify you are connected to the internet and the address {0} is correct.', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_serviceUnavailableMessage','Text', 'Genesys Unavailable', 48,getdate(),48,getdate())
  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_serviceUnavailableMessage','ux_textboxMessage', 'Genesys is currently unavailable.  Please try again later.', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_connectivityUnknownMessage','Text', 'Genesys Connection', 48,getdate(),48,getdate())
  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_connectivityUnknownMessage','ux_textboxMessage', 'You have encountered an unexpected issue connecting to Genesys. Please try again later.', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_dataviewNotFoundMessage','Text', 'Genesys Wizard Dataview Error', 48,getdate(),48,getdate())
  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','GenesysWizard_dataviewNotFoundMessage','ux_textboxMessage', 'The data view "{0}" is not part of GRINGlobal. Please add the dataview or change the name through Application Settings.', 48,getdate(),48,getdate())
  

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','GenesysWizard','Text', 'Asistente de Genesys v{0}', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','GenesysConfigWizard','Text', 'Configuración Genesys', 48,getdate(),48,getdate())
  
COMMIT TRANSACTION