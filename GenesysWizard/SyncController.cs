﻿using GeneSys2.Client;
using GeneSys2.Client.OAuth;
using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenesysWizard
{

    /// <summary>
    /// Type of Genesys Synchronization to perform
    /// </summary>
    public enum SyncType
    {
        Upsert = 0,
        Delete
    }

    /// <summary>
    /// Genesys Synchronization Controller
    /// </summary>
    public class SyncController : ISyncController
    {
        private SecureTokenStorage _storage;
        private OAuthHandler _authProvider = null;
        private SharedUtils _sharedUtils = null;

        /// <summary>
        /// SynchController constructor
        /// </summary>
        /// <param name="username">Username of the Curator user. Used to retrieve OAuth info</param>
        public SyncController(SharedUtils sharedUtils)
        {
            _sharedUtils = sharedUtils;

            // Tokens are stored in an encrypted file per Curator user, allowing multiple Genesys accounts
            // for shared computers
            string tokenFile = string.Format("{0}\\GRIN-Global\\Curator Tool\\genesys_{1}.txt", System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), _sharedUtils.Username);
            _storage = new SecureTokenStorage(tokenFile);
            
            _authProvider = new OAuthHandler(_storage.ServerAddress.AbsoluteUri, _storage.ClientKey, _storage.ClientSecret, string.Empty, string.Empty, _storage);
        }

        /// <summary>
        /// OAuth token storage
        /// </summary>
        public SecureTokenStorage TokenStorage
        {
            get
            {
                return _storage;
            }
        }

        /// <summary>
        /// Authentication provider
        /// </summary>
        public OAuthHandler AuthProvider
        {
            get
            {
                return _authProvider;
            }
        }

        /// <summary>
        /// GRINGlobal Client Shared Utilities
        /// </summary>
        public SharedUtils SharedUtils
        {
            get
            {
                return _sharedUtils;
            }
        }

        /// <summary>
        /// Address of the Genesys server
        /// </summary>
        public string ServerAddress 
        { 
            get
            {
                return TokenStorage != null ? TokenStorage.ServerAddress.AbsoluteUri : null;
            }
        }


        /// <summary>
        /// Update the OAuth Client ID and Secret
        /// </summary>
        /// <param name="serverAddress">URI of the Genesys server</param>
        /// <param name="clientId">OAuth Client Id</param>
        /// <param name="clientSecret">OAuth Client Secret</param>
        public void UpdateClientID(Uri serverAddress, string clientId, string clientSecret)
        {
            TokenStorage.ServerAddress = serverAddress;
            TokenStorage.ClientKey = clientId;
            TokenStorage.ClientSecret = clientSecret;
            _authProvider = new OAuthHandler(TokenStorage.ServerAddress.AbsoluteUri, clientId, clientSecret, string.Empty, string.Empty, _storage);
        }

        /// <summary>
        /// Check whether there is connectivity to the Genesys server
        /// </summary>
        /// <returns></returns>
        public ConnectivityTestResult CheckConnectivity()
        {
            ConnectivityTestResult result = ConnectivityTestResult.Unknown;

            if (!(string.IsNullOrWhiteSpace(_storage.ClientKey)) 
                && _authProvider != null)
            {
                var origCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;

                IGenesysClient provider = new GenesysClient(_storage.ServerAddress.AbsoluteUri, _authProvider);
                try
                {
                    var user = provider.User.Me();
                    result = ConnectivityTestResult.Success;
                }
                catch (UnauthorizedAccessException)
                {
                    result = ConnectivityTestResult.Unauthorized;
                }
                catch (AggregateException e)
                {
                    Cursor.Current = origCursor;
                    var exceptionType = e.InnerExceptions[0].GetType();

                    if (exceptionType == typeof(UnauthorizedAccessException))
                        result = ConnectivityTestResult.Unauthorized;
                    else if (exceptionType == typeof(System.Net.Http.HttpRequestException))
                        result = ConnectivityTestResult.NoConnection;
                    else if (exceptionType == typeof(GeneSys2.Client.GenesysApiException))
                    {
                        switch (((GenesysApiException)e.InnerExceptions[0]).HttpStatusCode)
                        {
                            case System.Net.HttpStatusCode.ServiceUnavailable:
                                result = ConnectivityTestResult.ServiceUnavailable;
                                break;
                            default:
                                MessageBox.Show(e.InnerExceptions[0].Message);
                                break;
                        }
                    }
                    else
                        MessageBox.Show(e.InnerExceptions[0].Message);
                }
                catch (Exception e)
                {
                    Cursor.Current = origCursor;
                    MessageBox.Show(e.Message);
                }
                finally
                {
                    Cursor.Current = origCursor;
                }
            }

            return result;
        }

        /// <summary>
        /// Launch the configuration wizard to setup the Genesys OAuth parameters
        /// </summary>
        public void Configure()
        {
            GenesysConfigWizard cw = new GenesysConfigWizard(this);
            cw.StartPosition = FormStartPosition.CenterParent;
            cw.ShowDialog();
        }


        /// <summary>
        /// Checks whether sufficient data exists in the data to commit it to
        /// Genesys.  INSTCODE and ACCENUMB are required. For deletes, GENUS is
        /// also needed.
        /// </summary>
        /// <param name="data">Accession data to commit</param>
        /// <param name="mode">Upsert or delete</param>
        /// <returns></returns>
        private bool IsValidDataview(DataTable data, SyncType mode)
        {
            if (mode == SyncType.Upsert)
                return (data.Columns.Contains("INSTCODE") && data.Columns.Contains("ACCENUMB"));
            else
                return (data.Columns.Contains("INSTCODE") && data.Columns.Contains("ACCENUMB") && data.Columns.Contains("GENUS"));
        }
    }
}
