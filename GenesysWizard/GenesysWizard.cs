﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
//using GeneSys2.Client;
//using GeneSys2.Client.Model;
using GRINGlobal.Client.Common;
using Newtonsoft.Json;
using GeneSys2.Client;
using GeneSys2.Client.Model;
//using GeneSys2.Client;
//using GeneSys2.Client.Model;

namespace GenesysWizard
{
    /// <summary>
    /// Interface required by the GRINGlobal Curator Tool wizard framework
    /// </summary>
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class GenesysWizard : Form, IGRINGlobalDataWizard
    {
        /// <summary>
        /// Maximum number of records to commit to Genesys at once.
        /// </summary>
        const int BATCH_SIZE = 50;

        /// <summary>
        /// Accession IDs passed to the Wizard form
        /// </summary>
        private string _accessionRequestPKeys;

        /// <summary>
        /// Genesys Controller Class
        /// </summary>
        private ISyncController _controller;

        /// <summary>
        /// Stores data which will be sent to Genesys for the selected accessions
        /// </summary>
        private DataTable _data = null;

        /// <summary>
        /// Supports UI by providing feedback to the user in table
        /// </summary>
        private DataTable _statusData = null;

        /// <summary>
        /// Indicates whether user selected Upload or Delete
        /// </summary>
        private SyncType _mode;

        /// <summary>
        /// Dataview name containing Passport Data
        /// </summary>
        private string _dataviewName = "get_passport_data";

        /// <summary>
        /// Column name with accession number
        /// </summary>
        private readonly string _columnNameAccesionNumber = "accessionNumber";
        /// <summary>
        /// Column name with institute code
        /// </summary>
        private readonly string _columnNameInstitudCode = "instituteCode";
        /// <summary>
        /// Column to try convert in boolean
        /// </summary>
        private readonly List<string> _columnsBooleanValues = new List<string>() { "mlsStatus", "available" };

        private BackgroundWorker _bw = null;
        Cursor _origCursor = null;
        /// <summary>
        /// Provider mock for test
        /// </summary>
        private IGenesysClient _inyectProvider;


        #region IGRINGlobalDataWizard
        /// <summary>
        /// Name of the wizard displayed in the CuratorTool
        /// </summary>
        public string FormName
        {
            get
            {
                return GenesysResources.WizardName;
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                return new DataTable();
            }
        }

        /// <summary>
        /// Required key type
        /// </summary>
        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }
        #endregion

        public GenesysWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            if (sharedUtils != null)
            {
                this._controller = new SyncController(sharedUtils);
            }

            // Setup upload data feedback grid
            _statusData = new DataTable();
            var column = new DataColumn("ACCENUMB");
            column.Caption = "Accession Number";
            _statusData.Columns.Add(column);
            _statusData.Columns.Add("STATUS");
            _statusData.Columns.Add("ID");

            // Ignore all pkey tokens except the orderrequest_id pkeys...
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                if (pkeyToken.Split('=')[0].Trim().ToUpper() == ":ACCESSIONID") _accessionRequestPKeys = pkeyToken;
            }

            // Configure background worker
            _bw = new BackgroundWorker();
            _bw.WorkerReportsProgress = true;
            _bw.RunWorkerCompleted += UploadRunWorkerCompleted;
            _bw.ProgressChanged += UploadProgressChanged;
            _bw.DoWork += DoWork;
        }

        /// <summary>
        /// Allow injection of mockups
        /// </summary>
        /// <param name="con"></param>
        /// <param name="data"></param>
        /// <param name="provider"></param>
        public void TestInyect(ISyncController con, DataTable data, IGenesysClient provider)
        {
            _controller = con;
            _data = data;
            _inyectProvider = provider;
        }

        private void GenesysWizard_Load(object sender, EventArgs e)
        {
            pbProgress.Style = ProgressBarStyle.Marquee;

            // Update Controls based on Languages
            if (this.components != null && this.components.Components != null) _controller.SharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _controller.SharedUtils.UpdateControls(this.Controls, this.Name);

            // Get Dataview Name if Overwritten in App Settings
            var dataViewAppSetting = _controller.SharedUtils.GetAppSettingValue("GenesysWizard-DataView");
            if (!string.IsNullOrWhiteSpace(dataViewAppSetting))
                this._dataviewName = dataViewAppSetting;

            // Load data from dataview for selected accessions
            dgAccessions.DataSource = _statusData;
            var dataLoaded = LoadData(this._accessionRequestPKeys);

            // Check connectivity to Genesys
            var canCommunicate = _controller.CheckConnectivity() == ConnectivityTestResult.Success;
            btnUpload.Enabled = canCommunicate;
            btnDelete.Enabled = canCommunicate;

            // Ensure required passport data elements exist in the dataview
            if (dataLoaded)
                InterrogateData();
        }

        /// <summary>
        /// Upload button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnUpload_Click(object sender, EventArgs e)
        {
            if (!CheckConnectivity() || _bw.IsBusy)
                return;

            _mode = SyncType.Upsert;
            btnConfigure.Enabled = false;
            btnCancel.Enabled = false;
            btnUpload.Enabled = false;
            btnDelete.Enabled = false;
            lblUploading.Visible = true;
            pbProgress.Visible = true;

            lblRecordCount.Text = string.Format(GenesysResources.Records, 0, _data.DefaultView.Count);
            lblErrorCount.Text = string.Format(GenesysResources.Errors, 0);
            lblErrorCount.ForeColor = SystemColors.ControlText;

            // Change cursor to the wait cursor...
            _origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            _bw.RunWorkerAsync();
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnDelete_Click(object sender, EventArgs e)
        {
            if (!CheckConnectivity() || _bw.IsBusy)
                return;

            _mode = SyncType.Delete;
            btnConfigure.Enabled = false;
            btnCancel.Enabled = false;
            btnUpload.Enabled = false;
            btnDelete.Enabled = false;
            lblUploading.Visible = true;
            pbProgress.Visible = true;

            lblRecordCount.Text = string.Format(GenesysResources.Records, 0, _data.DefaultView.Count);
            lblErrorCount.Text = string.Format(GenesysResources.Errors, 0);
            lblErrorCount.ForeColor = SystemColors.ControlText;

            // Change cursor to the wait cursor...
            _origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            _bw.RunWorkerAsync();
        }

        /// <summary>
        /// Update progress of upload
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is UploadStatus)
            {
                UploadStatus status = (UploadStatus)e.UserState;

                lblRecordCount.Text = string.Format(GenesysResources.Records, status.Processed, status.Total);
                lblErrorCount.Text = string.Format(GenesysResources.Errors, status.Errors);
                if (status.Errors > 0)
                    lblErrorCount.ForeColor = Color.Red;
            }
            Cursor.Current = Cursors.WaitCursor;
        }

        /// <summary>
        /// Upload thread has completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnConfigure.Enabled = true;
            btnCancel.Enabled = true;
            btnUpload.Enabled = true;
            btnDelete.Enabled = true;
            lblUploading.Visible = false;
            pbProgress.Visible = false;

            // Restore cursor
            Cursor.Current = _origCursor;

            // Report Errors to User
            int errors = 0;
            if (e.Result is int)
                errors = (int)e.Result;
            MessageBox.Show(string.Format(_mode == SyncType.Upsert ? GenesysResources.UploadCompleted : GenesysResources.DeleteCompleted, errors), this.Text, MessageBoxButtons.OK, errors == 0 ? MessageBoxIcon.Information : MessageBoxIcon.Warning);
        }

        /// <summary>
        /// Cancel button clicked to close the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Configure the Genesys Wizard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfigure_Click(object sender, EventArgs e)
        {
            this._controller.Configure();
            var canCommunicate = _controller.CheckConnectivity() == ConnectivityTestResult.Success;
            btnUpload.Enabled = canCommunicate;
            btnDelete.Enabled = canCommunicate;
        }


        /// <summary>
        /// Load the passport records to the UI table
        /// </summary>
        /// <param name="parameters"></param>
        private bool LoadData(string parameters = "")
        {

            DataSet ds;
            // Get the accession table and bind it to the main form on the General tabpage...
            ds = _controller.SharedUtils.GetWebServiceData(_dataviewName, parameters, 0, 0);

            if (ds.Tables.Contains(_dataviewName))
            {
                _data = ds.Tables[_dataviewName].Copy();
                if (!_data.Columns.Contains("uploadstatus"))
                {
                    _data.Columns.Add("uploadstatus");
                }
                lblRecordCount.Text = string.Format(GenesysResources.Records, 0, _data.DefaultView.Count);
                lblErrorCount.Text = string.Format(GenesysResources.Errors, 0);
            } else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data view '{0}' is not part of GRINGlobal. Please add the dataview or change the name through Application Settings.", "Genesys Wizard Dataview Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "GenesysWizard_dataviewNotFoundMessage";
                _controller.SharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, _dataviewName);
                ggMessageBox.ShowDialog();
                this.Close();
            }

            return _data != null;
        }

        /// <summary>
        /// Reviews all the fields in the source data, identifying matches with passport fields
        /// </summary>
        private void InterrogateData()
        {
            string[] fields = new string[] {
                "instituteCode", _columnNameAccesionNumber, "coll_collNumb", "coll_collMissId"
                , "taxonomy_genus","taxonomy_species","taxonomy_spAuthor", "taxonomy_subtaxa"
                , "taxonomy_subtAuthor", "ACCENAME","acquisitionDate","origCty","coll_collSite"
                ,"geo_latitude","geo_longitude","geo_uncertainty", "geo_datum", "geo_method"
                , "geo_elevation","coll_collDate","breederCode", "sampStat","ancest","coll_collSrc"
                ,"donorCode","donorName","donorNumb","OTHERNUMB", "mlsStatus", "acceUrl"
            };
            string[] fieldsContaint = new string[] {
                "coll_collCodeACD", "coll_collNameACD","coll_collInstAddressACD","duplSiteACD"
                ,"storageACD", "remarksACD"
            };
            var fieldsMissing = new List<string>();

            foreach (string field in fields)
            {
                if (!_data.Columns.Contains(field))
                    fieldsMissing.Add(field);
            }

            foreach (string field in fieldsContaint)
            {
                if (!_data.Columns.Contains(field))
                    fieldsMissing.Add(field);
            }

            _statusData.Rows.Clear();
            if (_data.DefaultView.ToTable().Columns.Contains("accession_id") && _data.DefaultView.ToTable().Columns.Contains(_columnNameAccesionNumber))
            {
                foreach (DataRowView row in _data.DefaultView)
                {
                    _statusData.Rows.Add(row[_columnNameAccesionNumber].ToString(), string.Empty, row["accession_id"]);
                }
            }

            lblRecordCount.Text = string.Format(GenesysResources.Records, 0, _data.DefaultView.Count);
            lblErrorCount.Text = string.Format(GenesysResources.Errors, 0);

            // If missing fields, notify user and prevent upload/download
            if (fieldsMissing.Count > 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data view '{0}' is missing the following required passport elements: \n\n      {1}\n\nGenesys data transfer will not be possible.", "Genesys Wizard Dataview Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "GenesysWizard_missingPassportFieldsMessage";
                _controller.SharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, _dataviewName, string.Join("\n      ", fieldsMissing));
                ggMessageBox.ShowDialog();
                this.Close();
            }
        }

        /// <summary>
        /// Verify that there is connectivity to Genesys and that the user is authorized
        /// </summary>
        /// <returns></returns>
        private bool CheckConnectivity()
        {
            var connectivityStatus = _controller.CheckConnectivity();
            if (connectivityStatus == ConnectivityTestResult.Success)
            {
                btnUpload.Enabled = true;
                btnDelete.Enabled = true;
                return true;
            } else
            {
                btnUpload.Enabled = false;
                btnDelete.Enabled = false;
            }

            // Connectivity problem. Provide details to the user.
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = null;
            switch (connectivityStatus)
            {
                case ConnectivityTestResult.Unauthorized:
                    ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Your Genesys connection has not been configured or you are no longer authorized. Please select Configure to resolve.", "Genesys Connection Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "GenesysWizard_connectivityUnauthorizedMessage";
                    _controller.SharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    break;
                case ConnectivityTestResult.NoConnection:
                    ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("No connection to Genesys. Please verify you are connected to the internet and the address {0} is correct.", "Genesys Connection Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "GenesysWizard_connectivityNoConnectionMessage";
                    _controller.SharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, _controller.ServerAddress);
                    break;
                case ConnectivityTestResult.ServiceUnavailable:
                    ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Genesys is currently unavailable.  Please try again later.", "Genesys Unavailable", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "GenesysWizard_serviceUnavailableMessage";
                    _controller.SharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    break;
                case ConnectivityTestResult.Unknown:
                default:
                    ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have encountered an unexpected issue connecting to Genesys. Please try again later.", "Genesys Connection", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "GenesysWizard_connectivityUnknownMessage";
                    _controller.SharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    break;
            }
            ggMessageBox.ShowDialog();

            return false;
        }

        /// <summary>
        /// Background Thread entry function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoWork(object sender, DoWorkEventArgs e)
        {
            if (_mode == SyncType.Delete)
                this.DeleteAccessionData(sender, e);
            else
                this.UploadAccessionData(sender, e);
        }

        /// <summary>
        /// Upload Accession Data to Genesys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UploadAccessionData(object sender, DoWorkEventArgs e)
        {
            // Name the thread for easier debugging
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Gensys Upload Worker";

            // Clear prior Results
            foreach (DataRow row in _statusData.Rows)
                row[1] = string.Empty;

            // Create Genesys Connection
            IGenesysClient provider = new GenesysClient(_controller.TokenStorage.ServerAddress.AbsoluteUri, _controller.AuthProvider);


            List<AccessionIdentifier> acceIds = new List<AccessionIdentifier>();
            List<GenericObject> accessions = new List<GenericObject>();
            List<AccessionNames> accessionNames = new List<AccessionNames>();
            int rowIdx = 0;
            int errorCount = 0;
            int recordsProcessed = 0;
            int recordsToProcess = _data.DefaultView.Count;
            AccessionIdComparer accessionComparer = new AccessionIdComparer();

            try
            {
                // Process Each Row in the Dataview
                foreach (DataRowView row in _data.DefaultView)
                {
                    // Map from Row to Accession
                    GenericObject acc = MapAccessionRow(row);
                    var newAccessId = new AccessionIdentifier()
                    {
                        InstCode = acc.ContainsKey(_columnNameInstitudCode) ? acc[_columnNameInstitudCode].ToString() : string.Empty,
                        AcceNumb = acc.ContainsKey(_columnNameAccesionNumber) ? acc[_columnNameAccesionNumber].ToString() : string.Empty,
                        Taxonomy = acc.ContainsKey("taxonomy")  ? new TaxonomyIdentifier() { Genus = row["taxonomy_genus"].ToString() } : null,
                    };

                    // Verify not a duplicate Accession
                    if (acceIds.Contains(newAccessId, accessionComparer))
                    {
                        // Duplicate!
                        _statusData.Rows[rowIdx]["STATUS"] = GenesysResources.Error_Duplicate;
                        ++errorCount;
                        ++recordsProcessed;
                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }
                    else
                    {
                        // Not a duplicate, so add to the list to upload
                        acceIds.Add(newAccessId);
                        accessions.Add(acc);

                        // Map from Row to AccessionNames
                        var names = MapAccessionNamesRow(row);
                        if (names != null)
                            accessionNames.Add(names);
                    }

                    // Upload the batch
                    if (rowIdx + 1 == recordsToProcess || accessions.Count == BATCH_SIZE)
                    {
                        // Upload
                        int resultCount = UploadBatch(provider, accessions, ref errorCount);
                        //UploadBatch(provider, accessionNames, ref errorCount);
                        recordsProcessed += resultCount;

                        // Clear batch list
                        accessions.Clear();
                        accessionNames.Clear();

                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }

                    rowIdx++;
                }
            }
            catch (Exception ex)
            {
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;

                MessageBox.Show(string.Format(GenesysResources.UnexpectedError, currentEx.Message), GenesysResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                e.Result = errorCount;
            }
        }

        /// <summary>
        /// Delete accession data from Genesys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteAccessionData(object sender, DoWorkEventArgs e)
        {
            // Name thread for easier debugging
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Gensys Delete Worker";

            // Clear prior Results
            foreach (DataRow row in _statusData.Rows)
                row[1] = string.Empty;

            // Create connection to Genesys
            IGenesysClient provider = new GenesysClient(_controller.TokenStorage.ServerAddress.AbsoluteUri, _controller.AuthProvider);

            List<AccessionIdentifier> acceIds = new List<AccessionIdentifier>();
            List<GenericObject> accessions = new List<GenericObject>();
            int rowIdx = 0;
            int errorCount = 0;
            int recordsProcessed = 0;
            int recordsToProcess = _data.DefaultView.Count;

            try
            {
                // Collect array of Accessions to Delete
                foreach (DataRowView row in _data.DefaultView)
                {
                    GenericObject acc = new GenericObject();

                    // Map required attributes for deletion
                    if (_data.Columns.Contains(_columnNameInstitudCode) && row[_columnNameInstitudCode] != null)
                        acc[_columnNameInstitudCode] = row[_columnNameInstitudCode].ToString();
                    if (_data.Columns.Contains(_columnNameAccesionNumber) && row[_columnNameAccesionNumber] != null)
                        acc[_columnNameAccesionNumber] = row[_columnNameAccesionNumber].ToString();
                    if (_data.Columns.Contains("taxonomy_genus") && row["taxonomy_genus"] != null)
                        acc["taxonomy"] = new Dictionary<string, string>() { { "genus", row["taxonomy_genus"].ToString() } };
                    if (_data.Columns.Contains("DOI") && row["DOI"] != null)
                        acc["doi"] = row["DOI"].ToString();
                    // Marking as "historic" will soft-delete the item in Genesys
                    acc["historic"] = true;

                    // Verify not a duplicate Accession
                    var newAccessId = new AccessionIdentifier()
                    {
                        InstCode = acc.ContainsKey("instCode") ? acc["instCode"].ToString() : string.Empty,
                        AcceNumb = acc.ContainsKey("acceNumb") ? acc["acceNumb"].ToString() : string.Empty,
                        Doi = acc.ContainsKey("doi") ? acc["doi"].ToString() : string.Empty,
                        Taxonomy = acc.ContainsKey("taxonomy") ? new TaxonomyIdentifier() {  Genus = row["taxonomy_genus"].ToString() } : null,
                    };
                    if (acceIds.Contains(newAccessId, new AccessionIdComparer()))
                    {
                        _statusData.Rows[rowIdx]["STATUS"] = GenesysResources.Error_Duplicate;
                        ++errorCount;
                        ++recordsProcessed;
                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }
                    else
                    {
                        acceIds.Add(newAccessId);
                        accessions.Add(acc);
                    }

                    // Upload batch
                    if (rowIdx + 1 == recordsToProcess || accessions.Count == BATCH_SIZE)
                    {
                        // Upload
                        int resultCount = UploadBatch(provider, accessions, ref errorCount);
                        recordsProcessed += resultCount;

                        // Clear batch list
                        accessions.Clear();

                        _bw.ReportProgress(100 * recordsProcessed / _data.DefaultView.Count, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = recordsToProcess });
                    }

                    ++rowIdx;
                }
            }
            catch (Exception ex)
            {
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;

                MessageBox.Show(string.Format(GenesysResources.UnexpectedError, currentEx.Message), GenesysResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                e.Result = errorCount;
            }
        }

        /// <summary>
        /// Upload a batch of records.  If there is error resulting from the batch command, all the 
        /// accessions in the batch will be marked as having an error.
        /// </summary>
        /// <param name="provider">Genesys connection</param>
        /// <param name="accessions">Batch of accessions</param>
        /// <param name="errorCount">Resulting error count</param>
        /// <returns>Number of accessions processed</returns>
        private int UploadBatch(IGenesysClient provider, IEnumerable<GenericObject> accessions, ref int errorCount)
        {
            if (accessions == null || accessions.Count() == 0)
                return 0;

            if(_inyectProvider != null)
            {
                provider = _inyectProvider;
            }

            IEnumerable<AccessionOpResponse> results = null;

            try
            {
                string uri = _mode == SyncType.Upsert ? "/acn/{0}/upsert" : "/acn/{0}/delete";
                // Submit accessions to Genesys
                results = JsonConvert.DeserializeObject<AccessionOpResponse[]>(
                        provider.Accession.UpdateJson(accessions.ElementAt(0)[_columnNameInstitudCode].ToString(), accessions.ToArray(), uri)
                    );
            }
            catch (Exception ex)
            {
                // Find the inner exception
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;
                // Exception writing the batch of accessions. Log errors for all.
                var batchResults = new List<AccessionOpResponse>();
                foreach (GenericObject acc in accessions)
                {
                    batchResults.Add(
                         new AccessionOpResponse()
                         {
                             AcceNumb = acc["acceNumb"].ToString(),
                             Taxonomy = new TaxonomyIdentifier () { Genus = acc["genus"].ToString() } ,
                             InstCode = acc["instCode"].ToString(),
                             Error = string.Format("{0}: {1}", GenesysResources.BatchError, currentEx.Message)
                         }
                     );
                }
                results = batchResults;
            }

            // Update the status data with the per-accession results
            foreach (AccessionOpResponse result in results)
            {
                var statusRow = _statusData.Select(string.Format("ACCENUMB='{0}' AND STATUS <> '{1}'", result.AcceNumb, GenesysResources.Error_Duplicate));
                if (statusRow.Length > 0)
                {
                    if (result.HasError)
                    {
                        statusRow[0]["STATUS"] = result.Error;
                        ++errorCount;
                    }
                    else
                    {
                        statusRow[0]["STATUS"] = _mode == SyncType.Upsert ? GenesysResources.UploadSuccess : GenesysResources.DeleteSuccess;
                    }
                }
            }

            return results.Count();
        }

        /// <summary>
        /// Upload a batch of Accession names
        /// </summary>
        /// <param name="provider">Genesys connection</param>
        /// <param name="accessionNames">Collection of AccessionNames</param>
        /// <param name="errorCount">Error count</param>
        /// <returns>Number of records processed</returns>
        private int UploadBatch(IGenesysClient provider, IEnumerable<AccessionNames> accessionNames, ref int errorCount)
        {
            if (accessionNames == null || accessionNames.Count() == 0)
                return 0;

            // Submit accession names to Genesys
            var results = provider.Accession.UpdateNames(accessionNames.ElementAt(0).InstCode, accessionNames.ToArray());

            // Update the accession status with the results of the call
            foreach (AccessionOpResponse result in results)
            {
                var statusRow = _statusData.Select(string.Format("ACCENUMB='{0}' AND STATUS <> '{1}'", result.AcceNumb, GenesysResources.Error_Duplicate));
                if (statusRow != null && statusRow.Length > 0)
                    if (result.HasError)
                    {
                        // If there is an error, replace a successful entry or append 
                        // to an existing error.
                        string currentStatus = statusRow[0]["STATUS"].ToString();

                        if (string.Compare(currentStatus, GenesysResources.UploadSuccess, true) == 0 || string.Compare(currentStatus, GenesysResources.DeleteSuccess, true) == 0)
                        {
                            statusRow[0]["STATUS"] = result.Error;
                            ++errorCount;
                        }
                        else
                        {
                            statusRow[0]["STATUS"] = string.Format("{0}. {1}", currentStatus, result.Error);
                        }
                    }
            }

            return results.Count();
        }

        /// <summary>
        /// Given a data row, returns an accession that represents the information
        /// </summary>
        /// <param name="row">Row of accession data</param>
        /// <returns>Accession object</returns>
        private GenericObject MapAccessionRow(DataRowView row)
        {
            // Fields that will not get mapped
            string[] ignoreFields = new string[] { "accession_id", "OTHERNUMB", "uploadstatus" };
            char prop_delim = '_';
            string array_indicator = "ACD";
            char array_delim = ';';

            // Create Generic object to store
            GenericObject accObj = new GenericObject();
            accObj.Add("historic", false); // In case an existing accession has been deleted (marked historic), uploads will always unmark 
            for(int colIdx = 0; colIdx < _data.Columns.Count; colIdx++)
            {
                // Don't process ignored fields
                if (ignoreFields.Contains(_data.Columns[colIdx].ColumnName))
                    continue;

                // Split column name to determine value location in object heirarchy
                string[] parts = _data.Columns[colIdx].ColumnName.Split(prop_delim);

                // Get correct dictionary to add property
                var targetObj = accObj;
                int currentPart = 0;
                while(currentPart + 1 < parts.Length)
                {
                    if (!targetObj.ContainsKey(parts[currentPart]))
                    {
                        targetObj.Add(parts[currentPart], new GenericObject());
                    }

                    targetObj = targetObj[parts[currentPart]] as GenericObject;
                    ++currentPart;
                }

                // Add property
                if (targetObj != null)
                {
                    //Search in the column name if contain the Array Column Definition ACD
                    if (parts[parts.Length - 1].Contains(array_indicator) )
                    {
                        //insert empty array [] in case the value is not set
                        targetObj.Add(parts[parts.Length - 1].Replace(array_indicator, string.Empty), row[colIdx].ToString().Split(new[] { array_delim }, StringSplitOptions.RemoveEmptyEntries));
                    }
                    else
                    {
                        //Find if there is a column type number that should be boolean and convert this value
                        if (_columnsBooleanValues.Contains(parts[parts.Length - 1]) && _data.Columns[colIdx].DataType == typeof(Int32))
                        {
                            targetObj.Add(parts[parts.Length - 1], row[colIdx].ToString() == "1");
                        }
                        else
                        {
                            targetObj.Add(parts[parts.Length - 1], row[colIdx]);
                        }
                    }
                }
            }
            return accObj;
        }

        /// <summary>
        /// Given a data row, returns an AccessionNames object
        /// </summary>
        /// <param name="row">Data row</param>
        /// <returns>AccessionNames object</returns>
        private AccessionNames MapAccessionNamesRow(DataRowView row)
        {
            // Process Accession Name Aliases(ACCENAME, OTHERNUMB)
            if ((_data.Columns.Contains(_columnNameAccesionNumber) && row[_columnNameAccesionNumber] != null) ||
                (_data.Columns.Contains("OTHERNUMB") && row["OTHERNUMB"] != null))
            {
                List<AccessionAlias> aliases = new List<AccessionAlias>();
                var instCode = row[_columnNameInstitudCode].ToString();
                var acceNumb = row[_columnNameAccesionNumber].ToString();

                if (_data.Columns.Contains(_columnNameAccesionNumber) && row[_columnNameAccesionNumber] != null)
                    aliases.Add(new AccessionAlias() { InstCode = instCode, Name = row[_columnNameAccesionNumber].ToString(), Type = AccessionAliasType.ACCENAME });
                if (_data.Columns.Contains("OTHERNUMB") && row["OTHERNUMB"] != null)
                    aliases.Add(new AccessionAlias() { InstCode = instCode, Name = row["OTHERNUMB"].ToString(), Type = AccessionAliasType.OTHERNUMB });

                return (new AccessionNames() { InstCode = instCode, AcceNumb = acceNumb, Aliases = aliases.ToArray() });
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Helper class providing a mechanism to pass progress info back to the
        /// UI thread.
        /// </summary>
        private class UploadStatus
        {
            /// <summary>
            /// Number of errors
            /// </summary>
            public int Errors { get; set; }

            /// <summary>
            /// Number of records processed
            /// </summary>
            public int Processed { get; set; }

            /// <summary>
            /// Total number of records to process
            /// </summary>
            public int Total { get; set; }
        }
    }
}
