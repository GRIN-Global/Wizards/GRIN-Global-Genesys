﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using GRINGlobal.Client.Common;
using System.Windows.Forms;
using System.Data;
using GeneSys2.Client.OAuth;
using System.Threading.Tasks;
using System.ComponentModel;
using GeneSys2.Client;
using GeneSys2.Client.Api;
using GeneSys2.Client.Model;
using System.Collections.Generic;

namespace GenesysWizard.Test
{
    [TestClass]
    public class GenesyWizardDesktopTest
    {
        /// <summary>
        /// Test when Grin Global Curator Tool wearch the Genesys Wizard
        /// </summary>
        [TestMethod]
        public void TestSearchWizard()
        {
            // Arrange
            string pkey = "";

            // Act
            GenesysWizard win = new GenesysWizard(pkey, null);

            // Assert
            Assert.IsNotNull(win);
        }

        /// <summary>
        /// Test when Genesys Wizard recives 0 array data in update accession data
        /// </summary>
        [TestMethod]
        public void TestUpdateWizardWithRow0()
        {
            // Arrange
            string pkey = "";
            DataTable passportData = GetDataPassport(0);
            var sync = new Mock<ISyncController>();
            sync.Setup(f => f.CheckConnectivity()).Returns(ConnectivityTestResult.Success);
            sync.SetupGet(f => f.AuthProvider).Returns(GetOAuthHandler());
            sync.SetupGet(f => f.TokenStorage).Returns(GetSecureTokenStorage());

            // Act
            GenesysWizard win = new GenesysWizard(pkey, null);
            win.TestInyect(sync.Object, passportData, GetIGenesysClient());
            win.UploadAccessionData(null, new DoWorkEventArgs(null));

            // Assert
            Assert.IsNotNull(win);
            Assert.AreEqual(win.lblErrorCount.Text, "Errors: 0");
        }
        /// <summary>
        /// Test when Genesys Wizard recives 1 array data in update accession data
        /// </summary>
        [TestMethod]
        public void TestUpdateWizardWithRow()
        {
            // Arrange
            string pkey = "";
            DataTable passportData = GetDataPassport(1);
            var sync = new Mock<ISyncController>();
            sync.Setup(f => f.CheckConnectivity()).Returns(ConnectivityTestResult.Success);
            sync.SetupGet(f => f.AuthProvider).Returns(GetOAuthHandler());
            sync.SetupGet(f => f.TokenStorage).Returns(GetSecureTokenStorage());
            // Act
            GenesysWizard win = new GenesysWizard(pkey, null);
            win.TestInyect(sync.Object, passportData, GetIGenesysClient());
            win.UploadAccessionData(null, new DoWorkEventArgs(null));

            // Assert
            Assert.IsNotNull(win);
            Assert.AreEqual(win.lblErrorCount.Text, "Errors: 0");
        }

        /// <summary>
        /// Test when Genesys Wizard recives 1 error to conect the server data in update accession data
        /// </summary>
        [TestMethod]
        public void TestUpdateWizardError()
        {
            // Arrange
            string pkey = "";
            DataTable passportData = GetDataPassport(1);
            var sync = new Mock<ISyncController>();
            sync.Setup(f => f.CheckConnectivity()).Returns(ConnectivityTestResult.Success);
            sync.SetupGet(f => f.AuthProvider).Returns(GetOAuthHandler());
            sync.SetupGet(f => f.TokenStorage).Returns(GetSecureTokenStorage());
            // Act
            GenesysWizard win = new GenesysWizard(pkey, null);
            win.TestInyect(sync.Object, passportData, GetIGenesysClientError());
            win.UploadAccessionData(null, new DoWorkEventArgs(null));

            // Assert
            Assert.IsNotNull(win);
            Assert.AreEqual(win.lblErrorCount.Text, "Errors: 0");
        }

        /// <summary>
        /// Test when Genesys Wizard recives 0 array data in delete accession data
        /// </summary>
        [TestMethod]
        public void TestDeleteWizardWithRow0()
        {
            // Arrange
            string pkey = "";
            DataTable passportData = GetDataPassport(0);
            var sync = new Mock<ISyncController>();
            sync.Setup(f => f.CheckConnectivity()).Returns(ConnectivityTestResult.Success);
            sync.SetupGet(f => f.AuthProvider).Returns(GetOAuthHandler());
            sync.SetupGet(f => f.TokenStorage).Returns(GetSecureTokenStorage());

            // Act
            GenesysWizard win = new GenesysWizard(pkey, null);
            win.TestInyect(sync.Object, passportData, GetIGenesysClient());
            win.DeleteAccessionData(null, new DoWorkEventArgs(null));

            // Assert
            Assert.IsNotNull(win);
            Assert.AreEqual(win.lblErrorCount.Text, "Errors: 0");
        }
        /// <summary>
        /// Test when Genesys Wizard recives 1 array data in delete accession data
        /// </summary>
        [TestMethod]
        public void TestDeleteWizardWithRow()
        {
            // Arrange
            string pkey = "";
            DataTable passportData = GetDataPassport(1);
            var sync = new Mock<ISyncController>();
            sync.Setup(f => f.CheckConnectivity()).Returns(ConnectivityTestResult.Success);
            sync.SetupGet(f => f.AuthProvider).Returns(GetOAuthHandler());
            sync.SetupGet(f => f.TokenStorage).Returns(GetSecureTokenStorage());
            // Act
            GenesysWizard win = new GenesysWizard(pkey, null);
            win.TestInyect(sync.Object, passportData, GetIGenesysClient());
            win.DeleteAccessionData(null, new DoWorkEventArgs(null));

            // Assert
            Assert.IsNotNull(win);
            Assert.AreEqual(win.lblErrorCount.Text, "Errors: 0");
        }

        private DataTable GetDataPassport(int numRow)
        {
            DataTable data = new DataTable();
            data.TableName = "get_passport_data";
            data.Columns.Add("accession_id", typeof(Int32));
            data.Columns.Add("accessionNumber", typeof(String));
            data.Columns.Add("instituteCode", typeof(String));
            data.Columns.Add("taxonomy_genus", typeof(String));
            data.Columns.Add("coll_collNumb", typeof(String));
            data.Columns.Add("coll_collCode", typeof(String));
            data.Columns.Add("coll_collName", typeof(String));
            data.Columns.Add("coll_collInstAddress", typeof(String));
            data.Columns.Add("coll_collMissId", typeof(String));
            data.Columns.Add("taxonomy_species", typeof(String));
            data.Columns.Add("taxonomy_spAuthor", typeof(String));
            data.Columns.Add("taxonomy_subtaxa", typeof(String));
            data.Columns.Add("taxonomy_subtAuthor", typeof(String));
            data.Columns.Add("ACCENAME", typeof(String));
            data.Columns.Add("acquisitionDate", typeof(DateTime));
            data.Columns.Add("origCty", typeof(String));
            data.Columns.Add("coll_collSite", typeof(String));
            data.Columns.Add("geo_latitude", typeof(float));
            data.Columns.Add("geo_longitude", typeof(float));
            data.Columns.Add("geo_uncertainty", typeof(String));
            data.Columns.Add("geo_datum", typeof(String));
            data.Columns.Add("geo_method", typeof(String));
            data.Columns.Add("geo_elevation", typeof(float));
            data.Columns.Add("coll_collDate", typeof(String));
            data.Columns.Add("breederCode", typeof(String));
            data.Columns.Add("sampStat", typeof(String));
            data.Columns.Add("ancest", typeof(String));
            data.Columns.Add("coll_collSrc", typeof(String));
            data.Columns.Add("donorCode", typeof(String));
            data.Columns.Add("donorName", typeof(String));
            data.Columns.Add("donorNumb", typeof(String));
            data.Columns.Add("OTHERNUMB", typeof(String));
            data.Columns.Add("duplSite$", typeof(String));
            data.Columns.Add("storage$", typeof(String));
            data.Columns.Add("mlsStatus", typeof(String));
            data.Columns.Add("remarks$", typeof(String));
            data.Columns.Add("acceUrl", typeof(String));
            
            for(int i=0; i<numRow; i++)
            {
                DataRow dr = data.NewRow();
                foreach(DataColumn col in data.Columns)
                {
                    if (col.ColumnName.Contains("$"))
                    {

                    }else
                    {
                        switch (col.DataType.Name)
                        {
                            case "Int32":
                                dr[col.ColumnName] = Convert.ToInt32(Faker.RandomNumber.Next(1,100));
                                break;
                            case "String":
                                dr[col.ColumnName] = Faker.Name.FullName();
                                break;
                            case "DateTime":
                                dr[col.ColumnName] = DateTime.Now.AddDays(-Faker.RandomNumber.Next(1, 100));
                                break;
                            case "float":
                                dr[col.ColumnName] = Faker.RandomNumber.Next(1, 100) * 1.12;
                                break;
                        }
                    }
                }
                data.Rows.Add(dr);
            }
            return data;
        }

        private SecureTokenStorage GetSecureTokenStorage()
        {
            SecureTokenStorage security = new SecureTokenStorage("aaa");
            Uri serverAddres = new Uri("http://www.contoso.com/");
            security.InjectTest(serverAddres);
            return security;
        }

        private OAuthHandler GetOAuthHandler()
        {
            var token = new Mock<ITokenStorage>();
            OAuthHandler oauth = new OAuthHandler("www", "cli", "aaaaa", "home", "cope", token.Object);
            return oauth;
        }

        private IGenesysClient GetIGenesysClient()
        {
            var provider = new Mock<IGenesysClient>();
            var accession = new Mock<IAccessionApi>();
            string jsonSuccess = "[{\"instCode\":\"MEX002\",\"doi\":\"10.18730/AZHSM\",\"acceNumb\":null,\"genus\":\"Hordeum\",\"error\":null,\"result\":{\"action\":\"UPDATE\",\"uuid\":\"18ee165d-29f8-4812-9808-f2b90a587e9f\"}}]";
            accession.Setup(f => f.UpdateJson(It.IsAny<string>(), It.IsAny<GenericObject[]>(), It.IsAny<string>())).Returns(jsonSuccess);
            provider.SetupGet(f => f.Accession).Returns(accession.Object);
            return provider.Object;
        }

        private IGenesysClient GetIGenesysClientError()
        {
            var provider = new Mock<IGenesysClient>();
            var accession = new Mock<IAccessionApi>();
            string jsonSuccess = "[{\"instCode\":\"MEX002\",\"doi\":\"10.18730/AZHSM\",\"acceNumb\":null,\"genus\":\"Hordeum\",\"error\":\"error\",\"result\":{\"action\":\"ERROR\",\"uuid\":\"18ee165d-29f8-4812-9808-f2b90a587e9f\"}}]";
            accession.Setup(f => f.UpdateJson(It.IsAny<string>(), It.IsAny<GenericObject[]>(), It.IsAny<string>())).Returns(jsonSuccess);
           // provider.SetupGet(f => f.Accession).Returns(accession.Object);
            return provider.Object;
        }
    }
}
